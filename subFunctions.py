"""Sub-function module"""

# import packages
import os
import shutil

def formatOutputDir(outputDir):
    """Function to format output directory string"""

    if outputDir.endswith('/'):
        return outputDir[:-1]
    
    return outputDir

def makeDir(directoryPath):
    """Function to delete and recreate directories, "clean", directories"""

    # check if directory exists
    if os.path.isdir(directoryPath) is True:
        # delete directory
        shutil.rmtree(directoryPath)
        # replace directory
        os.makedirs(directoryPath)
    else:
        # create directory
        os.makedirs(directoryPath)	
    return

def killDsStore(listIn):
    """Function to kill all .DS_Store files in a list"""

    # define indexing
    idx = 0

    # loop through list and delete .DS_Store files
    for dummy in listIn:

        if listIn[idx].endswith(".DS_Store"):

            del listIn[idx]

            # step back indexing
            idx = idx - 1

        # update indexing
        idx = idx + 1

    return listIn

def grabPaths(inputDir):
    """Function to create a list of file paths in a given directory"""

    paths = os.listdir(inputDir)

    # Remove DS.Store from list
    paths = killDsStore(paths)

    return paths

