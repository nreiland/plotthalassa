"""Module for plotting thalassa output"""

# import packages
import sys
import matplotlib.pyplot as plt

# import settings
import settings

# import functions
import subFunctions
import individual

def main():
    """Main Function"""

    # make output directory
    outputDir = subFunctions.formatOutputDir(settings.outputDir)
    subFunctions.makeDir(outputDir)

    # individual mode
    if settings.mode == 1:

        # Keplerian elements
        if settings.elementType == 1:

            # include mean
            if settings.plotMean == 1:

                # individual plots
                if settings.plotType == 1:
                    individual.meanIndivKeplerian(settings.inputFile, settings.outputDir)
                
                # combined plots
                elif settings.plotType == 2:
                    individual.meanComboKeplerian(settings.inputFile, settings.outputDir)
            
            # no mean
            elif settings.plotMean == 2:

                # individual plots
                if settings.plotType == 1:
                    individual.indivKeplerian(settings.inputFile, settings.outputDir)
                
                # combined plots
                elif settings.plotType == 2:
                    individual.comboKeplerian(settings.inputFile, settings.outputDir)
    
        # Cartesian coordinates
        if settings.elementType == 2:
            individual.indivCartesian(settings.inputFile, settings.outputDir)

            # include mean
            if settings.plotMean == 1:

                # individual plots
                if settings.plotType == 1:
                    individual.meanIndivCartesian(settings.inputFile, settings.outputDir)
                
                # combined plots
                elif settings.plotType == 2:
                    individual.meanComboCartesian(settings.inputFile, settings.outputDir)
            
            # no mean
            elif settings.plotMean == 2:

                # individual plots
                if settings.plotType == 1:
                    individual.indivCartesian(settings.inputFile, settings.outputDir)
                
                # combined plots
                elif settings.plotType == 2:
                    individual.comboCartesian(settings.inputFile, settings.outputDir)

def indivPlot(inputFile, outputDir):
    """plot an individual set of elements"""
    # Keplerian elements
    if settings.elementType == 1:

        # include mean
        if settings.plotMean == 1:

            # individual plots
            if settings.plotType == 1:
                individual.meanIndivKeplerian(inputFile, outputDir)
            
            # combined plots
            elif settings.plotType == 2:
                individual.meanComboKeplerian(inputFile, outputDir)
        
        # no mean
        elif settings.plotMean == 2:

            # individual plots
            if settings.plotType == 1:
                individual.indivKeplerian(inputFile, outputDir)
            
            # combined plots
            elif settings.plotType == 2:
                individual.comboKeplerian(inputFile, outputDir)

    # Cartesian coordinates
    if settings.elementType == 2:
        individual.indivCartesian(inputFile, outputDir)

        # include mean
        if settings.plotMean == 1:

            # individual plots
            if settings.plotType == 1:
                individual.meanIndivCartesian(inputFile, outputDir)
            
            # combined plots
            elif settings.plotType == 2:
                individual.meanComboCartesian(inputFile, outputDir)
        
        # no mean
        elif settings.plotMean == 2:

            # individual plots
            if settings.plotType == 1:
                individual.indivCartesian(inputFile, outputDir)
            
            # combined plots
            elif settings.plotType == 2:
                individual.comboCartesian(inputFile, outputDir)
    
# map command line arguments to function arguments
if __name__ == '__main__':

 	main(*sys.argv[1:])
