"""Settings module for plotThalassa script"""

# select mode (1: individual, 2: batch)
mode = 1

# select elements to plot (1: Keplerian, 2: Cartesian)
elementType = 1

# select plot output type (1: individual plots, 2: combined plots)
plotType = 2

# plot mean (1: mean plotted, 2: no mean plotted)
plotMean = 1

# output directory
outputDir = '/Users/nreiland/Documents/plots/test'

# INDIVIDUAL MODE
inputFile = '/Users/nreiland/Documents/oneWebPropagations/testProp/output/oneWeb3orbels.dat'

# BATCH MODE
inputDir = '/Users/nreiland/Documents/oneWebPropagations/10day/output'
