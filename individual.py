"""Module for plotting individual files for plotThalassa"""

# import packages
import matplotlib.pyplot as plt

def calcMean(element):
    """Function to fit a zeroth order polynomial (straight line) to the evolution of an orbital element"""
    n = len(element)
    val = 0
    for x in element:
        val = val + x
    return val/n

def meanList(element):
    """Function to create list of mean elements for horizontal line plotting"""

    n = len(element)
    meanList = [0,]*n
    mean = calcMean(element)
    for idx, dummy in enumerate(meanList, start = 0):
        meanList[idx] = mean

    return meanList

def parseThalassaOutput(inputPath):
    """Function to parse .dat file and return list of states"""

    # load data
    with open(inputPath, 'r') as fObj:
        lines = fObj.readlines()[3:]
    
    # parse data
    states = []
    for line in lines:
        line = line.split(',')[0:7]
        state = tuple(line)
        states.append(state)
    states = tuple(states)

    return states

def sortKepStates(states):
    """Function to sort keplerian orbital elements"""

    # create state lists
    t = []
    a = []
    e = []
    i = []
    W = []
    w = []
    M = []

    for x in states:
        t.append(float(x[0]))
        a.append(float(x[1]))
        e.append(float(x[2]))
        i.append(float(x[3]))
        W.append(float(x[4]))
        w.append(float(x[5]))
        M.append(float(x[6]))
    
    # convert lists to tuples
    t = tuple(t)
    a = tuple(a)
    e = tuple(e)
    i = tuple(i)
    W = tuple(W)
    w = tuple(w)
    M = tuple(M)

    # create dictionary output
    orbels = {}
    orbels['t'] = t
    orbels['a'] = a
    orbels['e'] = e
    orbels['i'] = i
    orbels['W'] = W
    orbels['w'] = w
    orbels['M'] = M

    return orbels

def sortCartStates(states):
    """Function to sort cartesian coodinates"""

    # create state lists
    t    = []
    X    = []
    Y    = []
    Z    = []
    Xdot = []
    Ydot = []
    Zdot = []

    for x in states:
        t.append(float(x[0]))
        X.append(float(x[1]))
        Y.append(float(x[2]))
        Z.append(float(x[3]))
        Xdot.append(float(x[4]))
        Ydot.append(float(x[5]))
        Zdot.append(float(x[6]))

    # convert lists to tuples
    t = tuple(t)
    X    = tuple(X)
    Y    = tuple(Y)
    Z    = tuple(Z)
    Xdot = tuple(Xdot)
    Ydot = tuple(Ydot)
    Zdot = tuple(Zdot)

    # create dictionary output
    carts = {}
    carts['t'] = t
    carts['X'] = X
    carts['Y'] = Y
    carts['Z'] = Z
    carts['Xdot'] = Xdot
    carts['Ydot'] = Ydot
    carts['Zdot'] = Zdot

    return carts


def indivCartesian(inputPath, outputDir):
    """Sub-function for plotting cartesian output"""

    # define output paths
    pathX    = '{}/X.png'.format(outputDir)
    pathY    = '{}/Y.png'.format(outputDir)
    pathZ    = '{}/Z.png'.format(outputDir)
    pathXdot = '{}/Xdot.png'.format(outputDir)
    pathYdot = '{}/Ydot.png'.format(outputDir)
    pathZdot = '{}/Zdot.png'.format(outputDir)

    # parse cart files
    states = parseThalassaOutput(inputPath)

    # convert states to coordinates
    carts = sortCartStates(states)

    # plot
    plt.figure(figsize=(20,20))
    plt.plot(carts['t'], carts['X'])
    plt.xlabel('MJD')
    plt.ylabel('X')
    plt.savefig(pathX)
    plt.close()
    
    plt.figure(figsize=(20,20))
    plt.plot(carts['t'], carts['Y'])
    plt.xlabel('MJD')
    plt.ylabel('Y')
    plt.savefig(pathY)
    plt.close()

    plt.figure(figsize=(20,20))
    plt.plot(carts['t'], carts['Z'])
    plt.xlabel('MJD')
    plt.ylabel('Z')
    plt.savefig(pathZ)
    plt.close()

    plt.figure(figsize=(20,20))
    plt.plot(carts['t'], carts['Xdot'])
    plt.xlabel('MJD')
    plt.ylabel('VX')
    plt.savefig(pathXdot)
    plt.close()

    plt.figure(figsize=(20,20))
    plt.plot(carts['t'], carts['Ydot'])
    plt.xlabel('MJD')
    plt.ylabel('VY')
    plt.savefig(pathYdot)
    plt.close()

    plt.figure(figsize=(20,20))
    plt.plot(carts['t'], carts['Zdot'])
    plt.xlabel('MJD')
    plt.ylabel('VZ')
    plt.savefig(pathZdot)
    plt.close()

def comboCartesian(inputPath, outputDir):
    """Function for plotting cartesian output on a combined plot"""

    # define output path
    cartPath = '{}/coordinates'.format(outputDir)

    # parse cart files
    states = parseThalassaOutput(inputPath)

    # convert states to coordinates
    carts = sortCartStates(states)

    # plot
    plt.figure(figsize=(26,20))
    plt.subplot(6, 1, 1)
    plt.plot(carts['t'], carts['X'])
    plt.ylabel('X')
    
    plt.subplot(6, 1, 2)
    plt.plot(carts['t'], carts['Y'])
    plt.ylabel('Y')

    plt.subplot(6, 1, 3)
    plt.plot(carts['t'], carts['Z'])
    plt.ylabel('Z')

    plt.subplot(6, 1, 4)
    plt.plot(carts['t'], carts['Xdot'])
    plt.ylabel('VX')

    plt.subplot(6, 1, 5)
    plt.plot(carts['t'], carts['Ydot'])
    plt.xlabel('MJD')
    plt.ylabel('VY')

    plt.subplot(6, 1, 6)
    plt.plot(carts['t'], carts['Zdot'])
    plt.xlabel('MJD')
    plt.ylabel('VZ')
    plt.savefig(cartPath)
    plt.close()

def indivKeplerian(inputPath, outputDir):
    """Sub-function for plotting Keplerian output on individual plots"""

    # define output paths
    pathSMA  = '{}/SMA.png'.format(outputDir)
    pathECC  = '{}/ECC.png'.format(outputDir)
    pathINC  = '{}/INC.png'.format(outputDir)
    pathRAAN = '{}/RAAN.png'.format(outputDir)
    pathARGP = '{}/ARGP.png'.format(outputDir)
    pathMA   = '{}/MA.png'.format(outputDir)

    # parse orbels files
    states = parseThalassaOutput(inputPath)

    # convert states to orbels
    orbels = sortKepStates(states)

    # plot
    plt.figure(figsize=(20,20))
    plt.plot(orbels['t'], orbels['a'])
    plt.xlabel('MJD')
    plt.ylabel('SMA')
    plt.savefig(pathSMA)
    plt.close()
    
    plt.figure(figsize=(20,20))
    plt.plot(orbels['t'], orbels['e'])
    plt.xlabel('MJD')
    plt.ylabel('ECC')
    plt.savefig(pathECC)
    plt.close()

    plt.figure(figsize=(20,20))
    plt.plot(orbels['t'], orbels['i'])
    plt.xlabel('MJD')
    plt.ylabel('INC')
    plt.savefig(pathINC)
    plt.close()

    plt.figure(figsize=(20,20))
    plt.plot(orbels['t'], orbels['W'])
    plt.xlabel('MJD')
    plt.ylabel('RAAN')
    plt.savefig(pathRAAN)
    plt.close()

    plt.figure(figsize=(20,20))
    plt.plot(orbels['t'], orbels['w'])
    plt.xlabel('MJD')
    plt.ylabel('ARGP')
    plt.savefig(pathARGP)
    plt.close()

    plt.figure(figsize=(20,20))
    plt.plot(orbels['t'], orbels['M'])
    plt.xlabel('MJD')
    plt.ylabel('MA')
    plt.savefig(pathMA)
    plt.close()

def comboKeplerian(inputPath, outputDir):
    """Function for plotting Keplerian output on a combined plot"""

    # define output path
    orbelsPath = '{}/orbels.png'.format(outputDir)

    # parse orbels files
    states = parseThalassaOutput(inputPath)

    # convert states to orbels
    orbels = sortKepStates(states)

    # plot
    plt.figure(figsize=(16,20))
    plt.subplot(6, 1, 1)
    plt.plot(orbels['t'], orbels['a'])
    plt.ylabel('SMA')
    
    plt.subplot(6, 1, 2)
    plt.plot(orbels['t'], orbels['a'])
    plt.ylabel('ECC')

    plt.subplot(6, 1, 3)
    plt.plot(orbels['t'], orbels['i'])
    plt.ylabel('INC')

    plt.subplot(6, 1, 4)
    plt.plot(orbels['t'], orbels['W'])
    plt.ylabel('RAAN')

    plt.subplot(6, 1, 5)
    plt.plot(orbels['t'], orbels['w'])
    plt.ylabel('ARGP')

    plt.subplot(6, 1, 6)
    plt.plot(orbels['t'], orbels['M'])
    plt.xlabel('MJD')
    plt.ylabel('MA')
    plt.savefig(orbelsPath)
    plt.close()

def meanIndivCartesian(inputPath, outputDir):
    """Sub-function for plotting cartesian output"""

    # define output paths
    pathX    = '{}/X.png'.format(outputDir)
    pathY    = '{}/Y.png'.format(outputDir)
    pathZ    = '{}/Z.png'.format(outputDir)
    pathXdot = '{}/Xdot.png'.format(outputDir)
    pathYdot = '{}/Ydot.png'.format(outputDir)
    pathZdot = '{}/Zdot.png'.format(outputDir)

    # parse cart files
    states = parseThalassaOutput(inputPath)

    # convert states to coordinates
    carts = sortCartStates(states)

    # plot
    plt.figure(figsize=(20,20))
    plt.plot(carts['t'], carts['X'])
    plt.plot(carts['t'], meanList(carts['X']))
    plt.xlabel('MJD')
    plt.ylabel('X')
    plt.savefig(pathX)
    plt.close()
    
    plt.figure(figsize=(20,20))
    plt.plot(carts['t'], carts['Y'])
    plt.plot(carts['t'], meanList(carts['Y']))
    plt.xlabel('MJD')
    plt.ylabel('Y')
    plt.savefig(pathY)
    plt.close()

    plt.figure(figsize=(20,20))
    plt.plot(carts['t'], carts['Z'])
    plt.plot(carts['t'], meanList(carts['Z']))
    plt.xlabel('MJD')
    plt.ylabel('Z')
    plt.savefig(pathZ)
    plt.close()

    plt.figure(figsize=(20,20))
    plt.plot(carts['t'], carts['Xdot'])
    plt.plot(carts['t'], meanList(carts['Xdot']))
    plt.xlabel('MJD')
    plt.ylabel('VX')
    plt.savefig(pathXdot)
    plt.close()

    plt.figure(figsize=(20,20))
    plt.plot(carts['t'], carts['Ydot'])
    plt.plot(carts['t'], meanList(carts['Ydot']))
    plt.xlabel('MJD')
    plt.ylabel('VY')
    plt.savefig(pathYdot)
    plt.close()

    plt.figure(figsize=(20,20))
    plt.plot(carts['t'], carts['Zdot'])
    plt.plot(carts['t'], meanList(carts['Zdot']))
    plt.xlabel('MJD')
    plt.ylabel('VZ')
    plt.savefig(pathZdot)
    plt.close()

def meanComboCartesian(inputPath, outputDir):
    """Function for plotting cartesian output on a combined plot"""

    # define output path
    cartPath = '{}/coordinates'.format(outputDir)

    # parse cart files
    states = parseThalassaOutput(inputPath)

    # convert states to coordinates
    carts = sortCartStates(states)

    # plot
    plt.figure(figsize=(26,20))
    plt.subplot(6, 1, 1)
    plt.plot(carts['t'], carts['X'])
    plt.plot(carts['t'], meanList(carts['X']))
    plt.ylabel('X')
    
    plt.subplot(6, 1, 2)
    plt.plot(carts['t'], carts['Y'])
    plt.plot(carts['t'], meanList(carts['Y']))
    plt.ylabel('Y')

    plt.subplot(6, 1, 3)
    plt.plot(carts['t'], carts['Z'])
    plt.plot(carts['t'], meanList(carts['Z']))
    plt.ylabel('Z')

    plt.subplot(6, 1, 4)
    plt.plot(carts['t'], carts['Xdot'])
    plt.plot(carts['t'], meanList(carts['Xdot']))
    plt.ylabel('VX')

    plt.subplot(6, 1, 5)
    plt.plot(carts['t'], carts['Ydot'])
    plt.plot(carts['t'], meanList(carts['Ydot']))
    plt.xlabel('MJD')
    plt.ylabel('VY')

    plt.subplot(6, 1, 6)
    plt.plot(carts['t'], carts['Zdot'])
    plt.plot(carts['t'], meanList(carts['Zdot']))
    plt.xlabel('MJD')
    plt.ylabel('VZ')
    plt.savefig(cartPath)
    plt.close()

def meanIndivKeplerian(inputPath, outputDir):
    """Sub-function for plotting Keplerian output on individual plots"""

    # define output paths
    pathSMA  = '{}/SMA.png'.format(outputDir)
    pathECC  = '{}/ECC.png'.format(outputDir)
    pathINC  = '{}/INC.png'.format(outputDir)
    pathRAAN = '{}/RAAN.png'.format(outputDir)
    pathARGP = '{}/ARGP.png'.format(outputDir)
    pathMA   = '{}/MA.png'.format(outputDir)

    # parse orbels files
    states = parseThalassaOutput(inputPath)

    # convert states to orbels
    orbels = sortKepStates(states)

    # plot
    plt.figure(figsize=(20,20))
    plt.plot(orbels['t'], orbels['a'])
    plt.plot(orbels['t'], meanList(orbels['a']))
    plt.xlabel('MJD')
    plt.ylabel('SMA')
    plt.savefig(pathSMA)
    plt.close()
    
    plt.figure(figsize=(20,20))
    plt.plot(orbels['t'], orbels['e'])
    plt.plot(orbels['t'], meanList(orbels['e']))
    plt.xlabel('MJD')
    plt.ylabel('ECC')
    plt.savefig(pathECC)
    plt.close()

    plt.figure(figsize=(20,20))
    plt.plot(orbels['t'], orbels['i'])
    plt.plot(orbels['t'], meanList(orbels['i']))
    plt.xlabel('MJD')
    plt.ylabel('INC')
    plt.savefig(pathINC)
    plt.close()

    plt.figure(figsize=(20,20))
    plt.plot(orbels['t'], orbels['W'])
    plt.plot(orbels['t'], meanList(orbels['W']))
    plt.xlabel('MJD')
    plt.ylabel('RAAN')
    plt.savefig(pathRAAN)
    plt.close()

    plt.figure(figsize=(20,20))
    plt.plot(orbels['t'], orbels['w'])
    plt.plot(orbels['t'], meanList(orbels['w']))
    plt.xlabel('MJD')
    plt.ylabel('ARGP')
    plt.savefig(pathARGP)
    plt.close()

    plt.figure(figsize=(20,20))
    plt.plot(orbels['t'], orbels['M'])
    plt.plot(orbels['t'], meanList(orbels['M']))
    plt.xlabel('MJD')
    plt.ylabel('MA')
    plt.savefig(pathMA)
    plt.close()

def meanComboKeplerian(inputPath, outputDir):
    """Function for plotting Keplerian output on a combined plot"""

    # define output path
    orbelsPath = '{}/orbels.png'.format(outputDir)

    # parse orbels files
    states = parseThalassaOutput(inputPath)

    # convert states to orbels
    orbels = sortKepStates(states)

    # plot
    plt.figure(figsize=(16,20))
    plt.subplot(6, 1, 1)
    plt.plot(orbels['t'], orbels['a'])
    plt.plot(orbels['t'], meanList(orbels['a']))
    plt.ylabel('SMA')
    
    plt.subplot(6, 1, 2)
    plt.plot(orbels['t'], orbels['e'])
    plt.plot(orbels['t'], meanList(orbels['e']))
    plt.ylabel('ECC')

    plt.subplot(6, 1, 3)
    plt.plot(orbels['t'], orbels['i'])
    plt.plot(orbels['t'], meanList(orbels['i']))
    plt.ylabel('INC')

    plt.subplot(6, 1, 4)
    plt.plot(orbels['t'], orbels['W'])
    plt.plot(orbels['t'], meanList(orbels['W']))
    plt.ylabel('RAAN')

    plt.subplot(6, 1, 5)
    plt.plot(orbels['t'], orbels['w'])
    plt.plot(orbels['t'], meanList(orbels['w']))
    plt.ylabel('ARGP')

    plt.subplot(6, 1, 6)
    plt.plot(orbels['t'], orbels['M'])
    plt.plot(orbels['t'], meanList(orbels['M']))
    plt.xlabel('MJD')
    plt.ylabel('MA')
    plt.savefig(orbelsPath)
    plt.close()
