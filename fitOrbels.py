"""Module for fitting a zeroth order polynomial to orbital elements"""

def calcMean(element):
    """Function to fit a zeroth order polynomial (straight line) to the evolution of an orbital element"""
    n = len(element)
    val = 0
    for x in element:
        val = val + x
    return val/n 


